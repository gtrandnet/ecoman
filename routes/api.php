<?php

use Illuminate\Http\Request;

Route::post('/register', 'Auth\AuthController@register');
Route::post('/login', 'Auth\AuthController@login');
Route::post('/logout', 'Auth\AuthController@logout');

Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('/me', 'Auth\AuthController@user');
    Route::get('/crm/home', 'Crm\CrmHomeController@index');
});

Route::post('/new_order', 'Crm\Orders\OrderController@newOrder');
Route::get('/orders', 'Crm\Orders\OrderController@index');