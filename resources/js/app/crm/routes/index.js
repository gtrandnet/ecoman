import { CrmHome } from '../components';
import { CrmNewOrders } from '../components';
import { NewOrder } from '../components';

export default [{
        path: '/',
        component: CrmHome,
        name: 'crm_home',
        // meta: {
        //     needsAuth: true
        // }
    },
    {
        path: '/crm/new/orders',
        components: { orders: NewOrder },
        name: 'crm_new_orders',
    }
]