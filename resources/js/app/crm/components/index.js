import Vue from 'vue'

export const CrmHome = Vue.component('crm-home-page', require('./CrmHome.vue').default)
export const CrmNewOrders = Vue.component('crm-new-orders', require('./CrmNewOrders.vue').default)
export const NewOrder = Vue.component('crm-new-orders', require('./orders/NewOrder.vue').default)