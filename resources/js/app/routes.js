import auth from './auth/routes'
import home from './home/routes'
import crm from './crm/routes'
//import errors from './errors/routes'

export default [...home, ...auth, ...crm]