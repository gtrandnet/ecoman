<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>ECON</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="css/app.css" rel="stylesheet">
        <!-- Place favicon.ico in the root directory -->
        <link rel="apple-touch-icon" href="{{asset('assets/images/pavicon.png')}}">
        <link rel="shortcut icon" type="image/ico" href="{{asset('assets/images/pavicon.png')}}" />
        <!-- Plugin-CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/normalize.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap-min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/owl-carousel-min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/lity-min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/font-awesome-min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}">
        <!-- Main-Stylesheets -->
        <link rel="stylesheet" href="{{asset('assets/css/helper.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/theme.css')}}">
        <link rel="stylesheet" href="{{asset('style.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
        <script src="{{asset('assets/js/vendor/modernizr-2-8-3-min.js')}}"></script>
    </head>
    <body data-spy="scroll" data-target=".mainmenu-area">
        <div id="app">
            <app></app>
        </div>
        <script src="js/app.js"></script>
        <!--Vendor-JS-->
        <script src="{{asset('assets/js/vendor/jquery-1-12-4-min.js')}}"></script>
        <!--Plugin-JS-->
        <script src="{{asset('assets/js/vendor/bootstrap-min.js')}}"></script>
        <script src="{{asset('assets/js/owl-carousel-min.js')}}"></script>
        <script src="{{asset('assets/js/ajaxchimp.js')}}"></script>
        <script src="{{asset('assets/js/lity-min.js')}}"></script>
        <script src="{{asset('assets/js/mouse-effect.js')}}"></script>
        <script src="{{asset('assets/js/scrollUp-min.js')}}"></script>
        <script src="{{asset('assets/js/wow-min.js')}}"></script>
        <!--Main-active-JS-->
        <script src="{{asset('assets/js/main.js')}}"></script>
    </body>
</html>
