<?php

namespace App\Crm\Orders;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['name', 'address', 'number'];
}
