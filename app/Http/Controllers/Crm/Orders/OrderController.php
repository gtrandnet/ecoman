<?php

namespace App\Http\Controllers\Crm\Orders;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Crm\Orders\Order;

class OrderController extends Controller
{
    public function index()
    {
        $order = Order::all();

        return response()->json($order, 200);
    }
    
    public function newOrder(OrderRequest $request)
    {
        $order = Order::create([
            'name' => $request->name,
            'address' => $request->address,
            'number' => $request->number
        ]);

        return response()->json([
            'data' => $order
        ], 200);
    }
}
